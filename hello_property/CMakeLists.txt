
cmake_minimum_required(VERSION 2.8.3)
project(hello_property)

## C++ 11
set ( CMAKE_CXX_STANDARD 11 )
set ( CMAKE_CXX_EXTENSIONS False )

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
 mauve_runtime
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  LIBRARIES
  CATKIN_DEPENDS mauve_runtime
#  DEPENDS system_lib
)

###########
## Build ##
###########

include_directories(
  ${catkin_INCLUDE_DIRS}
)

add_executable ( hello_shell_property src/hello_shell_property.cpp )
target_link_libraries ( hello_shell_property ${catkin_LIBRARIES} )

add_executable ( hello_core_property src/hello_core_property.cpp )
target_link_libraries ( hello_core_property ${catkin_LIBRARIES} )

add_executable ( hello_fsm_property src/hello_fsm_property.cpp )
target_link_libraries ( hello_fsm_property ${catkin_LIBRARIES} )
