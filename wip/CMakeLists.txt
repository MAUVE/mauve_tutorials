
cmake_minimum_required(VERSION 2.8.3)
project(wip)

## C++ 11
set ( CMAKE_CXX_STANDARD 11 )
set ( CMAKE_CXX_EXTENSIONS False )

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
 mauve_runtime
 simulation_tutorial
 guiding_tutorial
 navigation_tutorial
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
#  LIBRARIES wip
  CATKIN_DEPENDS mauve_runtime simulation_tutorial guiding_tutorial navigation_tutorial
#  DEPENDS system_lib
)

###########
## Build ##
###########

include_directories(
  include
  include/wip
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library with your components and architectures
set ( COM_SRC src/Communication.cpp src/CommunicationDeployer.cpp )
add_library ( communication ${COM_SRC} )
target_link_libraries ( communication ${catkin_LIBRARIES} )

set ( MAP_SRC src/Map.cpp src/Mapping.cpp src/Exploration.cpp )
add_library ( mapping ${MAP_SRC} )
target_link_libraries ( mapping ${catkin_LIBRARIES} )

set ( SONAR_SRC src/Sonar.cpp )
add_library ( sonar ${SONAR_SRC} )
target_link_libraries ( sonar ${catkin_LIBRARIES} )

## Declare a C++ executable, e.g. for a test file
add_executable ( com_test src/com_test.cpp )
target_link_libraries ( com_test communication )

## Declare a C++ library with your python deployer
add_library ( tutorial src/TutorialDeployer.cpp )
target_link_libraries ( tutorial communication mapping sonar )
