Navigating in the Grid Tutorial
===============================

## Table of contents

- [Tutorial presentation](#tutorial-presentation)
- [Creating a Catkin package](#creating-a-catkin-package)
- [Implementing the Navigation component](#implementing-the-navigation-component)
  - [Navigation shell](#navigation-shell)
  - [Navigation core](#navigation-core)
  - [Navigation FSM](#navigation-satte-machine)
- [The Navigation architecture](#the-navigation-architecture)
- [Create a Python Deployer](#create-a-python-deployer)
- [Solution](#solution)

## Tutorial presentation

In this tutorial, we will add a *Navigation* component that takes a goal as input, and computes a *path*, i.e. a sequence of targets close to each-other, that are sent to the *Guidance*.

![Navigation architecture](figures/navigation-architecture.png)

In this tutorial, you will not be guided along the implementation as in the [previous tutorial](#GuidingGridRobot.md). You will only have some high-level instructions to implement the component and the architecture. You can inspire from the previous tutorial, or in extreme cases, go and look at the [Solution](#solution).

## Creating a Catking package

First, create a new `navigation_tutorial` package that depends on `mauve_runtime`, `simulation_tutorial` and `guiding_tutorial`.

## Implementing the Navigation component

### Navigation shell

The *Navigation* component has three ports:
* an input port *goal* of type `Cell`
* an input port *pose* of type `Pose`
* an output port *target* of type `Cell`

### Navigation core

In the *Navigation* core, it is advised to define the following attributes:
* a *goal*
* a *pose*
* a *path* as a vector of cells
* a boolean representing that a *new_goal* has been received
* an index representing the current target within the path

Moreover, define the following methods:
* a _read_ method that read input ports, and check that a new goal has been received;
* a *compute_path* method that fills the *path* vector according to the current pose and goal; implement the algorithm you want; for instance, in the solution, the path is computed by first moving the robot only on the *i* axis, and then on the *j* axis;
* a *execute_path* method that first check if the robot is arrived at the current target, and if arrived, writes the next target. Add a log message when you send a new target to be able to check the execution of your component.

### Navigation state-machine

We will implement a multi-clock FSM for the *Navigation* component. Let's consider that the *compute_path* method could take some time to execute (even if this is not the case in this tutorial), for instance if we implement a search-based algorithm on a large map.

We will then define a FSM that has a short period when executing the path, in order to be more reactive, and a larger period when computing a new path.

The state-machine should look like this:

![Navigation FSM](figures/navigation-psm.png)

In the *Read* state, read the input ports. In the *Plan* state, compute the path. In the *Exec* state, execute the path.
If you have received a new goal, go from *Read* to *Plan*.
Otherwise, if you are have a target to execute, go from *Read* to *Exec*.
Otherwise, just loop on *Read* with a period of 1s.

## The Navigation architecture

Inherit from the *Guiding Tutorial* architecture, and add the *Navigation* component, the *goal* resource, and make the connection accoring to the figure:

![Navigation architecture](figures/navigation-architecture.png)

Set the initial value of *target* to (0,0) to avoid sending guidance commands.
Set the initial value of *goal* to, e.g., (3,5) to have a first path be sent to the *Navigation* component.

## Create a Python Deployer

Create a Python deployer to manipulate your architecture, and test it!

## Solution

The `mauve_tutorials` repository includes the `navigation_tutorial` package completed.
You can either look at the source files to help you go on in the tutorial.
If you want to compile the solution instead of your pacakge, move the file `CATKIN_IGNORE` from the solution directory to your directory:
```bash
cd ~/mauve_ws
mv src/mauve_tutorials/navigation_tutorial/CATKIN_IGNORE src/navigation_tutorial/CATKIN_IGNORE
catkin_make
```
