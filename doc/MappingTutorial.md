Mapping the Grid Tutorial
=========================

## Table of contents

- [Tutorial presentation](#tutorial-presentation)
- [Creating a Catkin package](#creating-a-catkin-package)
- [Solution](#solution)

## Tutorial presentation

In this tutorial, we will add a *Mapping* component that takes poses and scans, and updates a *map*, managed
by a specific resource. We will also implement a *Communication* component to receive goals from a remote
operator using a 3rd party *libcom* library.

![Mapping architecture](figures/mappingarchitecture.png)

In this tutorial, you will not be guided along the implementation.
You will only have some high-level instructions to implement the components and the architecture.
 You can inspire from the previous tutorials, or in extreme cases, go and look at the [Solution](#solution).

## Creating a Catking package

First, create a new `mapping_tutorial` package that depends on `mauve_runtime`,
 `simulation_tutorial`, `navigation_tutorial` and `libcom`.

## Implementing the Map resource


## Solution

The `mauve_tutorials` repository includes the `mapping_tutorial` package completed.
You can either look at the source files to help you go on in the tutorial.
If you want to compile the solution instead of your pacakge, move the file `CATKIN_IGNORE` from the solution directory to your directory:
```bash
cd ~/mauve_ws
mv src/mauve_tutorials/mapping_tutorial/CATKIN_IGNORE src/mapping_tutorial/CATKIN_IGNORE
catkin_make
```
