#ifndef MAUVE_TUTORIAL_LIBCOM_HPP
#define MAUVE_TUTORIAL_LIBCOM_HPP

#include <sys/select.h>
#include <vector>

namespace libcom {

  struct Server {
    std::vector<int> clients;
    int server;
    fd_set sockets;
    char buffer[80];
    bool open_socket(int port);
    void register_client(int fd);
    bool recv();
  }; // struct

} // namespace

#endif // MAUVE_TUTORIAL_LIBCOM_HPP
