#include "mauve/runtime.hpp"
#include <iostream>
#include <string>

using namespace mauve::runtime;

struct HelloShell: public Shell {
  // Create a property in the shell named "Who"
  Property<std::string> & who = mk_property<std::string>("who", "World");
};

struct HelloCore: public Core<HelloShell> {
  // Create a property in the core named "N"
  Property<int> & N = mk_property("N", 1);

  void update() {
    // The shell property is used in the core
    std::string who = shell().who;
    // The core property is also used in the core
    for (int i = 1; i <= N; i++) {
      std::cout << "Hello " << who << " ! ";
    }
    std::cout << std::endl;
  }

  void smile() {
    std::cout << ":-)\n";
  }
};

struct HelloFSM: public FiniteStateMachine<HelloShell, HelloCore> {
  // Create the Execution state: run the update method of the core
  ExecState<HelloCore>    & U = mk_execution("Update", &HelloCore::update);
  // Create the Execution state: run the smile method of the core
  ExecState<HelloCore>    & S = mk_execution("Smile" , &HelloCore::smile);
  // Create the synchronization state: 1 sec
  SynchroState<HelloCore> & W = mk_synchronization("Wait", sec_to_ns(1));
  // Create a FSM property named smile
  Property<bool> & smile = mk_property<bool>("smile", false);

  bool configure_hook() override {
    set_initial(U); // set the initial state to Update
    // The FSM property is used
    if (smile) {
      set_next(U, S); // Smile after Update
    }
    else {
      set_next(U, W); // Wait after Update
    }
    set_next(S, W); // Wait after Smile
    set_next(W, U); // Update after Wait
    return true;
  }
};

struct HelloArchi: public Architecture {
  // Create the component based on the previously defined Shell, Core, FSM
  Component<HelloShell, HelloCore, HelloFSM> & hello_cpt = mk_component<HelloShell, HelloCore, HelloFSM>("hello_cpt");

  bool configure_hook() override {
    // Change the Shell property value
    hello_cpt.shell().who = "you";
    // Change the Core property value
    hello_cpt.core().N = 5;
    // Change the property of the FSM
    hello_cpt.fsm().smile = true;
    // Configure the component
    hello_cpt.configure();
    return true;
  }
};

int main(int argc, char const *argv[]) {
  HelloArchi architecture;

  AbstractDeployer* deployer = mk_abstract_deployer(&architecture);
  architecture.configure();

  deployer->create_tasks();
  deployer->activate();
  deployer->start();

  deployer->loop();

  deployer->stop();

  return 0;
}
