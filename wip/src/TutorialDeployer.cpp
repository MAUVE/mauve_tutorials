#include "CommunicationArchitecture.hpp"
#include "Sonar.hpp"
#include "Exploration.hpp"
#include "Map.hpp"
#include "Mapping.hpp"

extern "C" void mk_python_deployer() {
  auto archi = new CommunicationArchitecture();

  using Exploration = mauve::Component<ExplorationShell, ExplorationCore, ExplorationFSM>;

  std::function<bool (mauve::Deployer<CommunicationArchitecture> *)>
    reconf_sonar = [](mauve::Deployer<CommunicationArchitecture>* depl)
  {
    auto laser = depl->get_architecture()->laser;
    auto laser_task = depl->get_task(laser);
    laser_task->stop();
    laser->cleanup_core();
    laser->replace_core<SonarCore>();
    laser->configure_core();
    laser_task->start(depl->now());
    return true;
  };

  std::function<bool (mauve::Deployer<CommunicationArchitecture> *)>
    reconf_explo = [](mauve::Deployer<CommunicationArchitecture>* depl)
  {
    auto com = depl->get_architecture()->communication;
    auto com_task = depl->get_task(com);
    com_task->stop();
    com->cleanup();

    Exploration* explo = depl->get_architecture()->
      mk_component<ExplorationShell, ExplorationCore, ExplorationFSM>("exploration");

    return true;
  };

  auto manager = new mauve::Manager<CommunicationArchitecture>();
  manager->save("reconf_sonar", reconf_sonar);
  manager->save("reconf_explo", reconf_explo);

  mauve::mk_deployer(archi, manager);
}
