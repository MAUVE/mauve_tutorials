#ifndef READER_HPP
#define READER_HPP


#include "mauve/runtime.hpp"
#include <iostream>

using namespace mauve::runtime;

/* --------------- Shell --------------- */

struct ReaderStatusShell: public Shell {
  // Create the input port of type int and named "input"
  ReadPort<StatusValue<int>> & input = mk_port<ReadPort<StatusValue<int>>>("input", -1);
};

/* --------------- Core --------------- */

struct ReaderStatusCore: public Core<ReaderStatusShell> {

  void update() {
    // Read the input Port
    StatusValue<int> sv = shell().input.read();
    DataStatus status = sv.status;
    int value = sv.value;
    // Display current count value
    std::cout << this->type_name() << "> status = " << status << " value = " << value << std::endl;
  }

};

/* --------------- Finite State Machine --------------- */

struct ReaderStatusFSM: public FiniteStateMachine<ReaderStatusShell, ReaderStatusCore> {
  // Create the Execution state: run the update method of the core
  ExecState<ReaderStatusCore>    & U = mk_execution("Update", &ReaderStatusCore::update);
  // Create the synchronization state: 1 sec
  SynchroState<ReaderStatusCore> & W = mk_synchronization("Wait", ms_to_ns(500));

  bool configure_hook() override {
    set_initial(U); // set the initial state to Update
    set_next(U, W); // Wait after Update
    set_next(W, U); // Update after Wait
    return true;
  }
};

#endif
