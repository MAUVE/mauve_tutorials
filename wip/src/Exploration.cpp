#include <tuple>

#include "Exploration.hpp"

void ExplorationCore::update() {
  using namespace simulation_tutorial;
  Pose p = shell().pose.read();

  if (current_goal == Cell{-1,-1})
    current_goal = p.c;

  if (p.c == current_goal) {
    logger->info("arrived at goal {}", current_goal);
    // find new goal
    int w, h;
    int distance = std::numeric_limits<int>::max();
    std::tie(w, h) = shell().map.size();
    for (int i = 0; i < w; i++)
      for (int j = 0; j < h; j++) {
        int d = abs(j-p.c.j) + abs(i-p.c.i);
        if (d < distance && shell().map.get({i,j}) == Occupancy::UNKNOWN) {
          distance = d;
          current_goal = {i, j};
        }
      }
    shell().goal.write(current_goal);
  }

}
