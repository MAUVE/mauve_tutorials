#ifndef MAUVE_TUTORIAL_EXPLORATION_HPP
#define MAUVE_TUTORIAL_EXPLORATION_HPP

#include <mauve_runtime/mauve_runtime.hpp>
#include <simulation_tutorial/Pose.hpp>
#include "Map.hpp"

struct ExplorationShell : public mauve::Shell {
  mauve::ReadPort<simulation_tutorial::Pose>& pose =
    mk_port< mauve::ReadPort<simulation_tutorial::Pose> >("pose", simulation_tutorial::Pose());
  mauve::WritePort<simulation_tutorial::Cell>& goal =
    mk_port< mauve::WritePort<simulation_tutorial::Cell> >("goal");
  MapPort& map = mk_port<MapPort>("map");
};

struct ExplorationCore : public mauve::Core<ExplorationShell> {
  simulation_tutorial::Cell current_goal;
  void update();
  bool configure_hook() {
    current_goal = {-1, -1};
    return true;
  }
};

struct ExplorationFSM : public mauve::FiniteStateMachine<ExplorationShell, ExplorationCore> {
  mauve::Execution<ExplorationCore>* e;
  mauve::Wait<ExplorationCore>* w;
  bool configure_hook() override {
    e = mk_execution("E", &ExplorationCore::update);
    w = mk_wait("W", mauve::sec_to_ns(10));
    set_initial(e);
    set_next(e, w);
    set_next(w, e);
    return true;
  };
};

#endif
