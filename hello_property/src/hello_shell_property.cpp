#include "mauve/runtime.hpp"
#include <iostream>
#include <string>

using namespace mauve::runtime;

struct HelloShell: public Shell {
  // Create a property in the shell named "Who"
  Property<std::string> & who = mk_property<std::string>("who", "World");
};

struct HelloCore: public Core<HelloShell> {

  void update() {
    std::string who = shell().who;
    std::cout << "Hello " << who << " !" << std::endl;
  }
};

struct HelloFSM: public FiniteStateMachine<HelloShell, HelloCore> {
  // Create the Execution state: run the update method of the core
  ExecState<HelloCore>    & U = mk_execution("Update", &HelloCore::update);
  // Create the synchronization state: 1 sec
  SynchroState<HelloCore> & W = mk_synchronization("Wait", sec_to_ns(1));

  bool configure_hook() override {
    set_initial(U); // set the initial state to Update
    set_next(U, W); // Wait after Update
    set_next(W, U); // Update after Wait
    return true;
  }
};

struct HelloArchi: public Architecture {
  // Create the component based on the previously defined Shell, Core, FSM
  Component<HelloShell, HelloCore, HelloFSM> & hello_cpt = mk_component<HelloShell, HelloCore, HelloFSM>("hello_cpt");

  bool configure_hook() override {
    // Change the Shell property value
    hello_cpt.shell().who = "you";
    // Configure the component
    hello_cpt.configure();
    return true;
  }
};

int main(int argc, char const *argv[]) {
  // Create the architecture
  HelloArchi architecture;

  // Create a deployer for the architecture
  AbstractDeployer* deployer = mk_abstract_deployer(&architecture);

  // Configure the architecture
  architecture.configure();

  // Create the component task
  deployer->create_tasks();
  // Activate the component task
  deployer->activate();

  // Start the deployer and the component task
  deployer->start();

  // Deployer is running until C-C
  deployer->loop();

  // Stop the deployer and the component task
  deployer->stop();

  return 0;
}
